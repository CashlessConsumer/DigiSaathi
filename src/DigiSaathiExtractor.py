import requests
import json

def write_json(new_data, filename):
    with open(filename, 'w') as file:
        json.dump(new_data, file, indent=4)

def GetProviderList():
  url = "https://digisaathi.info/npciAPI/bot/bankname"
  headers = {
    'appId': '8f1a9ebe-7ce5-4c3e-bf3b-18669e4897e3'
  }
  response = requests.request("GET", url, headers=headers)
  write_json(response.json(),'../data/Providers.json')


def ExtractDigiSaathiDetails():
  with open('../data/Providers.json', 'r') as f:
    providers = json.load(f)

  url = "https://digisaathi.info/npciAPI/bot/sendQueryFaq/en"
  headers = {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
    'Origin': 'https://digisaathi.info',
    'Pragma': 'no-cache',
    'Referer': 'https://digisaathi.info/',
    'appId': '8f1a9ebe-7ce5-4c3e-bf3b-18669e4897e3'
  }

  contact_details = []
  for provider in providers:
    contact_detail = {}
    payload = json.dumps({
      "query": provider,
      "source": "Website",
      "inputType": "Text",
      "isFallback": True,
      "userToken": None
    })

    response = requests.request("POST", url, headers=headers, data=payload)
    contact_detail['name'] = provider
    contact_detail['digisaathi_response'] = response.json()['answer']
    contact_details.append(contact_detail)

  write_json(contact_details,'../data/DigiSaathidata.json')


def ParseDigiSaathiData():
  with open('../data/DigiSaathidata.json', 'r') as f:
    digisaathi_response = json.load(f)

  entities = []

  for response in digisaathi_response:
    entity = {}
    entity['name'] = response['name'].replace('\u00a0', ' ')
    if 'You may please contact your Bank or institution' in response['digisaathi_response']:
      entity['phone'] = ''
      entity['web'] = ''
      entity['email'] = ''
    for detail in response['digisaathi_response'].split('</br>'):
      if 'Or you can visit' in detail:
        entity['web'] = detail.split('Or you can visit')[1].strip()
      if 'Or write to' in detail:
        entity['email'] = detail.split('Or write to')[1].strip().replace('\r\n', '').replace('\n', '')
      if 'You can contact at ' in detail:
        entity['phone'] = detail.split('You can contact at ')[1].strip().replace('\r\n', '').replace('\n', '').replace('.','')
    entities.append(entity)

  write_json(entities,'../data/DigiSaathiParsedData.json')

def main():
  GetProviderList()
  ExtractDigiSaathiDetails()
  ParseDigiSaathiData()

if __name__ == "__main__":
    main()