# DigiSaathi

This project aims to extract Opendata from DigiSaathi

[DigiSaathi](https://digisaathi.info/) – An initiative by the consortium of Payment System Operators and Participants (banks & non-banks) in India. The DigiSaathi initiative is being maintained by National Payments Corporation of India. This project aims to extract open data from the DigiSaathi service and attempts to make it available for public use.


## TODO

- [ ] Make the entire extraction fully automated.
    - [ ] Extract Providers
    - [ ] Map it with bank codes / BIN
- [ ] Publish data in friendly format
